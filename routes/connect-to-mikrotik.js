var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('connect-to-mikrotik', { title: 'Express' });
});

module.exports = router;
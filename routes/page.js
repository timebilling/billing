﻿const express = require('express');
const router = express.Router();
// const middleware = require('../helpers/middlewares');
// const fcmHelper = require('../helpers/fcmHelper');
const CONST = require('../helpers/constants');
// const checkHelper = require('../helpers/checkHelper');
// const langHelper = require('../helpers/langHelper');
// const mailHelper = require('../helpers/mailHelper');
// var models  = require('../models');


/* GET home page. */
router.get('/home', function(req, res, next) {
  models.constant.all().then(constants => {
    constInfo = {};
    for (var i = 0; i < constants.length; i++) {
        constInfo[constants[i].key] = constants[i].value;
    }
    res.render('index', constInfo);
  }).catch(err => {
    return next(err);
  });
});

/* GET home page. */
router.get('/landing', function(req, res, next) {
  models.constant.all().then(constants => {
    constInfo = {};
    for (var i = 0; i < constants.length; i++) {
      constInfo[constants[i].key] = constants[i].value;
    }
    res.render('index', constInfo);
  }).catch(err => {
    return next(err);
  });
});


/* GET chat page. */
router.post('/request', function(req, res, next) {
  // langHelper.trans('INCORRECT_PHONE')
  var to = ['info@fingram.kz','asara.me@gmail.com'];
  var subject = 'Новая заявка на займ';
  var text = 'Пользователь с номером телефона: ' + req.body.phone + '. \r\n';
      text += 'Сделал заявку на займ в размере ' + req.body.summa + ' тенге ';
      text += 'сроком на ' + req.body.period + ' дней.';
  console.log(text);
  var result = mailHelper.send(to, subject, text);
  console.log(result);

  return res.json({
    "message" : 'Ваша заявка отправлена!'
  });
});

router.post('/login', function(req, res, next) {
  return res.json({
    "message" : langHelper.trans('NOT_REGISTERED')
  });
});


/* GET chat page. */
router.get('/chat', function(req, res, next) {
  res.render('chat');
});


/* GET invite page. */
router.get('/dl', function(req, res, next) {
    var md = checkHelper.mobileDetect(req);
    var ip = req.header('x-forwarded-for') || req.connection.remoteAddress;

    console.log(' ======================= ', md);
    models.dl_analytics.create({
      browser: md.family,
      browser_version: md.os.major,
      device: md.device.family,
      device_version: md.device.major,
      os: md.os.family,
      os_version: md.os.major,
      ip_address:  ip,
    }).then(dl => {
      if(dl.os == 'Android') {
        res.writeHead(302, {
          'Location': 'https://play.google.com/store/apps/details?id=kz.fingram'
        });
        res.end();
      }else {
        models.constant.all().then(constants => {
          constInfo = {};
          for (var i = 0; i < constants.length; i++) {
              constInfo[constants[i].key] = constants[i].value;
          }
          res.render('index', constInfo);
           res.end();
        }).catch(err => {
          return next(err);
        });
      }
    }).catch(err => {
      return next(err);
    });
});

/* GET emotions page. */
router.get('/emotions', function(req, res, next) {
  models.constant.all().then(constants => {
    constInfo = {};
    for (var i = 0; i < constants.length; i++) {
        constInfo[constants[i].key] = constants[i].value;
    }
    res.render('emotions', constInfo);
  }).catch(err => {
    return next(err);
  });
});

/* GET kredity-online page. */
router.get('/kredity-online', function(req, res, next) {
  models.constant.all().then(constants => {
    constInfo = {};
    for (var i = 0; i < constants.length; i++) {
        constInfo[constants[i].key] = constants[i].value;
    }
    res.render('kredity-online', constInfo);
  }).catch(err => {
    return next(err);
  });
});
module.exports = router;
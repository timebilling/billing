module.exports = {
    TYPE_VERIFICATION: 1, // тип проверки отправки для проверки номера пользователя
    TYPE_INVITE: 2, // тип проверки отправки для проверки инвайта
    TYPE_RESET: 3, // тип проверки отправки для смены пароля

    SMS_LOGIN: 'fingram',
    SMS_PASSWORD: 'azsxdcfv_sms',

    SMS_STATUS: {
        '-3' : 'Сообщение не найдено',
        '-1' : 'Ожидает отправки',
        '0'  :'Передано оператору',
        '1'  :'Доставлено',
        '2'  :'Прочитано',
        '3'  :'Просрочено',
        '20' : 'Невозможно доставить',
        '22' : 'Неверный номер',
        '23' : 'Запрещено',
        '24' : 'Недостаточно средств',
        '25' : 'Недоступный номер',
    },

    OS_IOS: 1,
    OS_ANDROID: 2,
    OS_WINDOW: 3,
    OS_BLACKBERRY: 4,
    BROWSER: 5,

    VERIFICATION_STEP_1: 1,
    VERIFICATION_STEP_2: 2,
    VERIFICATION_STEP_3: 3,
    VERIFICATION_APPROVED: 4,
    VERIFICATION_BLOCKED: 5,
    VERIFICATION_DISAPPROVED: 6,

    ACTIVATE: 1,
    DEACTIVATE: 0,

    FCM_NOTIFY  : 'notification',
    FCM_PASS_RESET  : 'password_reset',
    FCM_TEAM_DONE  : 'team_done',

    TOTAL_MONTH : 2,
    DEFAULT_SALARY_DAY : 10,
    DAYS_TO_BUILD_TEAM : 4,
    DAYS_TO_PAY4USER : 2,
    DAYS_TO_PAY4US : 2,
    DAYS_TO_PAY : 2 + 2,


    PAYMENT_TYPE_CONTRIBUTION: 0,
    PAYMENT_TYPE_PAY : 1,
    PAYMENT_TYPE_FEE : 2,

    QIWI_IPS : [
        '1',
        '89.218.54.34',
        '89.218.54.36',
        '212.154.215.82',
        '79.142.55.227',
        '79.142.55.231',
        '185.146.1.121'
    ],
    QIWI_SERVER : '89.218.54.34',
    QIWI_RESERVE_SERVER : '89.218.54.34',
    QIWI_TEST_SERVER : '212.154.215.82',
    QIWI_RESERVE_TEST_SERVER : '79.142.55.227',

    QIWI_TYPE_CHECK : 'check',
    QIWI_TYPE_PAY : 'pay',


    QIWI_SUCCESS : 0, // ОК
    QIWI_ERROR_TEMPORARY : 1, // Временная ошибка. Повторите запрос позже
    QIWI_ERROR_INCORRECT_FORMAT_USER : 4, // Неверный формат идентификатора абонента +
    QIWI_ERROR_UNKNOWN_USER  : 5, // Идентификатор абонента не найден (Ошиблись номером) +
    QIWI_ERROR_BLOCK_PAYMENTS_PROVIDER : 7, // Прием платежа запрещен провайдером +
    QIWI_ERROR_BLOCK_PAYMENTS_TECHNICAL_CAUSE : 8, // Прием платежа запрещен по техническим причинам +
    QIWI_ERROR_USER_NOT_ACTIVE : 79, // Счет абонента не активен +
    QIWI_ERROR_PAYMENT_NOT_FINISHED : 90, // Проведение платежа не окончено
    QIWI_ERROR_PAY_SUM_LOW : 241, // Сумма слишком мала +
    QIWI_ERROR_PAY_SUM_LARGE : 242, // Сумма слишком велика +
    QIWI_ERROR_CANNOT_CHECK_ACCOUNT : 243, // Невозможно проверить состояние счета +
    QIWI_ERROR_PROVIDER_OTHER : 300, // Другая ошибка провайдера

    PERIOD_OF_TIME_DAYS : 'days',//Day offset. For example, every week a friend gets money
    WEEKS_PERIOD_OF_TIME : 'weeks',//Week offset. For example, every week a friend gets money
    MONTHS_PERIOD_OF_TIME : 'months',//Month offset. For example, every month a friend gets money
    
    YES : 1,//Boolean True
    NO : 0,//Boolean False
    // A user receives money on Friday
    FRIDAY_DAY_OF_WEEK : 5,//Friday. Thank God it's Friday!

    DATE_FORMAT_YYYY_MM_DD: 'YYYY-MM-DD',

    // VERIFICATION_ID : 4,// Id=4 in table 'verification_statuses' in DB
};

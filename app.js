var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var enIndexRouter = require('./routes/en_index');
var kzIndexRouter = require('./routes/kz_index');
var serverRequirementsRouter = require('./routes/server_requirements');
var userManualRouter = require('./routes/user-manual');
var createAbonentRouter = require('./routes/sozdat-abonenta');
var serviceManagementRouter = require('./routes/upravlenie-uslugami');
var serviceInvoiceRouter = require('./routes/realizaciya-uslug');
var armOperRouter = require('./routes/arm-operatora');
var adminManualRouter = require('./routes/admin-manual');
var synchraWorksRouter = require('./routes/synchra-works');
var connectToMikrotikRouter = require('./routes/connect-to-mikrotik');
var billingServicesRouter = require('./routes/billing-services');
var servicesPriceRouter = require('./routes/services-price');
var cashNonCashPaymentsRouter = require('./routes/cash-non-cash-payments');
var kaspiPaymentsRouter = require('./routes/kaspi-payments');
var filterSortRouter = require('./routes/filter-sort');
var billPlus1CRouter = require('./routes/billing-plus-1Cbuhgalter');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'pug');
app.set('view engine', 'ejs');

app.use(logger('dev'));
// app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
// app.use(favicon(path.join(__dirname, 'public/img/', 'favicon.png')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/en', enIndexRouter);
app.use('/kz', kzIndexRouter);
app.use('/server_requirements', serverRequirementsRouter);
app.use('/user-manual', userManualRouter);
app.use('/sozdat-abonenta', createAbonentRouter);
app.use('/upravlenie-uslugami', serviceManagementRouter);
app.use('/realizaciya-uslug', serviceInvoiceRouter);
app.use('/arm-operatora', armOperRouter);
app.use('/admin-manual', adminManualRouter);
app.use('/synchra-works', synchraWorksRouter);
app.use('/connect-to-mikrotik', connectToMikrotikRouter);
app.use('/billing-services', billingServicesRouter);
app.use('/services-price', servicesPriceRouter);
app.use('/cash-non-cash-payments', cashNonCashPaymentsRouter);
app.use('/kaspi-payments', kaspiPaymentsRouter);
app.use('/filter-sort', filterSortRouter);
app.use('/billing-plus-1Cbuhgalter', billPlus1CRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

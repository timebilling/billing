'use strict';
var gulp   = require('gulp'),
	apidoc = require('gulp-apidoc'),
    watch = require('gulp-watch'),
	rigger = require('gulp-rigger'),
    copy = require('gulp-contrib-copy'),
	prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    cssmin = require('gulp-minify-css');

    var path = {
	    build: { //Тут мы укажем куда складывать готовые после сборки файлы
	        js: './public/js/',
	        css: './public/css/',
	        fonts: './public/fonts/',
	    },
	    src: { //Пути откуда брать исходники
	        js: './assets/js/main.js',
	        style: './assets/style/main.scss',
	        fonts: './assets/fonts/**/*.*',
	    },
	    watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
	        js: './assets/js/**/*.js',
	        style: './assets/style/**/*.scss',
	        fonts: './assets/fonts/**/*.*',
	    }
	};

gulp.task('apidoc:build', function(done) {
    apidoc({
        src: './routes',
        dest: './apidoc',
        includeFilters: [ ".*\\.js$" ],
        debug: true,
    }, done);
});

gulp.task('js:build', function () {
    gulp.src(path.src.js) //Найдем наш main файл
        .pipe(rigger()) //Прогоним через rigger
        .pipe(sourcemaps.init()) //Инициализируем sourcemap
        .pipe(uglify()) //Сожмем наш js
        .pipe(sourcemaps.write()) //Пропишем карты
        .pipe(gulp.dest(path.build.js)); //Выплюнем готовый файл в build
});

gulp.task('style:build', function () {
    gulp.src(path.src.style) //Выберем наш main.scss
        .pipe(sourcemaps.init()) //То же самое что и с js
        .pipe(sass()) //Скомпилируем
        .pipe(prefixer()) //Добавим вендорные префиксы
        .pipe(cssmin()) //Сожмем
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(path.build.css)); //И в build
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts));
});

gulp.task('css:copy', function() {
    gulp.src([
      './node_modules/bootstrap3/dist/css/bootstrap.min.css',
      './node_modules/ion-rangeslider/css/ion.rangeSlider.css',
      './node_modules/ion-rangeslider/css/ion.rangeSlider.skinHTML5.css',
      './node_modules/fullpage.js/dist/jquery.fullpage.min.css',
      './node_modules/animate.css/animate.min.css',
    ])
    .pipe(copy())
    .pipe(gulp.dest("./public/css"));
});

gulp.task('js:copy', function() {
    gulp.src([
      './node_modules/bootstrap3/dist/js/bootstrap.min.js',
      './node_modules/scrollmagic/scrollmagic/minified/ScrollMagic.min.js',
      './node_modules/scrollmagic/scrollmagic/minified/plugins/debug.addIndicators.min.js',
      './node_modules/ion-rangeslider/js/ion.rangeSlider.min.js',
      './node_modules/jquery.maskedinput/src/jquery.maskedinput.js',
      './node_modules/fullpage.js/dist/jquery.fullpage.min.js',
    ])
    .pipe(copy())
    .pipe(gulp.dest("./public/js"));
});


// gulp.task('build', [
//     'js:build',
//     'style:build',
//     'fonts:build',
//     'apidoc:build',
//     'css:copy',
//     'js:copy'
// ]);
gulp.task('build', gulp.series(
        'js:build',
        'style:build',
        'fonts:build',
        'apidoc:build',
        'css:copy',
        'js:copy'
));


gulp.task('watch', function(){
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
    });
    watch('./routes/*.js', function() {
      gulp.run('apidoc:build');
   });
});

// gulp.task('default', ['build', 'watch']);
gulp.task('default', gulp.series('build', 'watch'));
// gulp.task('all-tasks', gulp.series('clean-app', 'clean-tests'));

